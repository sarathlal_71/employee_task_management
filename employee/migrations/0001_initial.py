# Generated by Django 2.2.5 on 2019-09-19 11:45

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='employee',
            fields=[
                ('emp_id', models.AutoField(primary_key=True, serialize=False)),
                ('emp_name', models.CharField(default='xss', max_length=100)),
                ('task', models.TextField(default='any apppp')),
                ('start_date', models.DateTimeField(blank=True, default=datetime.datetime.today, null=True)),
                ('finished_date', models.DateTimeField(blank=True, default=datetime.datetime.today, null=True)),
            ],
            options={
                'ordering': ['emp_id'],
            },
        ),
    ]
