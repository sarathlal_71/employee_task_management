from django.db import models
from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
from datetime import date,datetime

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted([(item, item) for item in get_all_styles()])



class employee(models.Model):
    emp_id = models.AutoField(primary_key=True)
    emp_name = models.CharField(max_length=100,  default='xss')
    task = models.TextField(default="any apppp")
    start_date = models.DateTimeField(blank=True, null=True,default=datetime.today)
    finished_date = models.DateTimeField(blank=True, null=True,default=datetime.today)


    class Meta:
        ordering = ['emp_id']