from rest_framework import serializers
from employee.models import employee, LANGUAGE_CHOICES, STYLE_CHOICES


class employeeSerializer(serializers.Serializer):
    emp_id = serializers.IntegerField(read_only=True)
    emp_name = serializers.CharField(required=False, allow_blank=True, max_length=100)
    task = serializers.CharField()
    start_date = serializers.DateTimeField(required=False)
    finished_date = serializers.DateTimeField()
    # style = serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')

    def create(self, validated_data):
        """
        Create and return a new `employee` instance, given the validated data.
        """
        return employee.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `employee` instance, given the validated data.
        """
        instance.emp_id = validated_data.get('emp_id', instance.emp_id)
        instance.emp_name = validated_data.get('emp_name', instance.emp_name)
        instance.task = validated_data.get('task', instance.task)
        instance.start_date = validated_data.get('start_date', instance.start_date)
        instance.finished_date = validated_data.get('finished_date', instance.finished_date)
        instance.save()
        return instance