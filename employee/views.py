from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from employee.models import employee
from employee.serializers import employeeSerializer

# Create your views here.
@csrf_exempt
def employee_list(request):
    """
    List all code employees, or create a new employee.
    """

    if request.method == 'GET':
        employees = employee.objects.all()
        print(employees)
        serializer = employeeSerializer(employees, many=True)

        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = employeeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def employee_detail(request, pk):
    """
    Retrieve, update or delete a code employee.
    """
    try:
        employee = employee.objects.get(pk=pk)
    except employee.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = employeeSerializer(employee)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = employeeSerializer(employee, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        employee.delete()
        return HttpResponse(status=204)