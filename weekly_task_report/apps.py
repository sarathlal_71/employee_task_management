from django.apps import AppConfig


class WeeklyTaskReportConfig(AppConfig):
    name = 'weekly_task_report'
