from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
# Create your views here.
from celery import shared_task
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from employee.models import employee
import pandas as pd
from datetime import date,datetime,timedelta
import os


def weekly_report_generate(x):
    today_date = datetime.today()
    print(today_date)
    week_day = today_date - timedelta(8)
    print(week_day.date())
    employee_all = list(employee.objects.filter(start_date__range = [week_day.date(), today_date.date()]).all().values())#["2019-09-11", "2019-09-19"]
    print(employee_all)
    df = pd.DataFrame(employee_all)
    curr_dir = os.getcwd()
    file_name=  str(week_day.date())+'-'+str(today_date.date())
    if os.path.exists(curr_dir+'/../'+file_name+'.csv'):
        os.remove(curr_dir+'/../'+file_name+'.csv')
    export_csv = df.to_csv(curr_dir+'/../'+file_name+'.csv', index=None, header=True)
    # print(employee_all)
    print("celeryy")
    # fo = open("/home/sarathlal/Documents/raw/weekly_report.csv","w")
    # fo.write(employee_all[0])
    # fo.close()
    return JsonResponse("weekly report generate",safe=False)

